# learningscala-dev

Essentially a blog as I learn Scala.  Published to https://www.learningscala.dev

## Local development

Use `sbt run`, and then access the app at `http://localhost:9000`