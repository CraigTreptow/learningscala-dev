# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.


## [0.1.2] - 2021-04-25

### Fixed

- About link
## [0.1.1] - 2021-04-25

### Added

- Numbers addition

## [0.1.0] - 2021-04-24

### Added

- About page

## [0.0.1] - 2021-04-22

### Added

- Initial release deployed to Heroku
